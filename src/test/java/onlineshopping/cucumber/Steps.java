package onlineshopping.cucumber;

import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import onlineshopping.CartItem;
import onlineshopping.CatalogItem;

public class Steps {

	@Given("the following users:")
	public void the_followin_users(List<String> userIds) {
	}

	@Given("there are no items in the catalog")
	public void there_are_no_items_in_the_catalog() {
	}

	@When("user {string} requests catalog")
	public void user_requests_catalog(String string) {
	}

	@Then("an empty catalog is retuned")
	public void an_empty_catalog_is_retuned() {
	}

	@Given("the catalog contains the following items:")
	public void the_catalog_contains_the_following_items(List<CatalogItem> items) {
	}

	@Then("the following catalog items are retuned:")
	public void the_following_catalog_items_are_retuned(List<CatalogItem> items) {
	}

	@Given("cart of {string} is empty")
	public void cart_of_is_empty(String userId) {
	}

	@When("user {string} adds {string} to cart")
	public void user_adds_to_cart(String userId, String itemId) {
	}

	@Then("cart of {string} should contain the following items:")
	public void cart_of_should_contain_the_following_items(String userId, List<CartItem> items) {
	}

	@Given("cart of {string} contains the following items:")
	public void cart_of_contains_the_following_items(String userId, List<CartItem> items) {
	}

}