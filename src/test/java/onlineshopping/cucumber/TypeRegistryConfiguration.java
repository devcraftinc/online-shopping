package onlineshopping.cucumber;

import static java.lang.Integer.parseInt;

import java.util.Locale;
import java.util.Map;

import cucumber.api.TypeRegistry;
import cucumber.api.TypeRegistryConfigurer;
import io.cucumber.datatable.DataTableType;
import onlineshopping.CartItem;
import onlineshopping.CatalogItem;

public class TypeRegistryConfiguration implements TypeRegistryConfigurer {

	@Override
	public Locale locale() {
		return Locale.ENGLISH;
	}

	@Override
	public void configureTypeRegistry(TypeRegistry typeRegistry) {
		typeRegistry.defineDataTableType(new DataTableType(CatalogItem.class, (Map<String, String> row) -> {
			CatalogItem item = new CatalogItem();
			item.setId(row.get("Item Id"));
			item.setName(row.get("Item Name"));
			return item;
		}));
		typeRegistry.defineDataTableType(new DataTableType(CartItem.class, (Map<String, String> row) -> {
			CartItem item = new CartItem();
			item.setId(row.get("Item Id"));
			item.setName(row.get("Item Name"));
			item.setQuantity(parseInt(row.get("Quantity")));
			return item;
		}));
	}
}