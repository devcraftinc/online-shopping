Feature: item selection
  I want to select products for checkout

  Background: 
    Given the following users:
      | U1 |
      | U2 |

  Scenario: add item to cart
    Given the catalog contains the following items:
      | Item Id | Item Name  |
      | item1   | Skype      |
      | item2   | Office 365 |
    And cart of "U1" is empty
    When user "U1" adds "item1" to cart
    Then cart of "U1" should contain the following items:
      | Item Id | Item Name | Quantity |
      | item1   | Skype     |        1 |

 	 Scenario: add item that is already in cart
    Given the catalog contains the following items:
      | Item Id | Item Name  |
      | item1   | Skype      |
      | item2   | Office 365 |
    And cart of "U1" contains the following items:
      | Item Id | Item Name | Quantity |
      | item1   | Skype     |        1 |
    When user "U1" adds "item1" to cart
    Then cart of "U1" should contain the following items:
      | Item Id | Item Name | Quantity |
      | item1   | Skype     |        1 |