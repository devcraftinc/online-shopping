Feature: view catalog
  I want view catalog so I can select the products I want purchase

  Background: 
    Given the following users:
      | U1 |
      | U2 |

  Scenario: view empty catalog
    Given there are no items in the catalog
    When user "U1" requests catalog
    Then an empty catalog is retuned

  Scenario: view catalog with items
    Given the catalog contains the following items:
      | Item Id | Item Name  |
      | item1   | Skype      |
      | item2   | Office 365 |
    When user "U1" requests catalog
    Then the following catalog items are retuned:
      | Item Id | Item Name  |
      | item1   | Skype      |
      | item2   | Office 365 |
